// console.log("Quicksort");
class Person {
    constructor(name, age, salary, sex) {
        this.name = name;
        this.age = age;
        this.salary = salary;
        this.sex = sex;
    }
    // static print(arr) {
    //     for (let i = 0; i < arr.length; i++) {
    //         console.log(arr[i].name);
    //     }
    // }

    static sort(array, field, order) {

        if (array.length <= 1) {
            return array;
        }

        if (field === "male") {
            const result = array.filter(arr => arr["sex"] === "male");
            return result;
        }
        if (field === "female") {
            const result = array.filter(arr => arr["sex"] === "female");
            return result;
        }
        const pivot = [array.length - 1];
        //    console.log("Pivot is",pivot)

        let smaller = [];
        let middle = [array[pivot]];
        let bigger = [];
        for (let i = 0; i < array.length; i++) {
            if (i == pivot) {
                continue;
            }
            if (array[i][field] === array[pivot][field]) {
                middle.push(array[i]);

            }
            else if (array[i][field] < array[pivot][field]) {
                smaller.push(array[i]);

            }
            else {
                bigger.push(array[i]);
            }
            // console.log("Element is",array[i][field]);
        }
        if (order === "asc") {

            const newarr = [...this.sort(smaller, field, order), ...middle, ...this.sort(bigger, field, order)];
            return newarr;
        }
        else if (order === "desc") {

            const newarr = [...this.sort(bigger, field, order), ...middle, ...this.sort(smaller, field, order)];
            return newarr;
        }
    }
}

const p1 = new Person("nand", 21, 15000, "male");
const p2 = new Person("nittin", 23, 158000, "male");
const p3 = new Person("berlin", 29, 15000, "male");
const p4 = new Person("tokyo", 23, 15080, "female");
const p5 = new Person("alicia", 22, 54008, "female");
const p6 = new Person("professor", 35, 18000, "male");
const p7 = new Person("nairobi", 35, 18000, "female");

// ARRAY OF PERSON OBJECTS
const p = [p1, p2, p3, p4, p5, p6, p7];
// const p = [p1];


// console.log(p);
// console.log("*********before*********************");
// console.log(Person.sort(p,"name"));
// console.log("*********After*********************");
// console.log(p);


//Sorted array
console.log("***********************Sorted Array***********************************");
console.log(Person.sort(p, "name", "asc"));
// console.log(Person.sort(p, "name", "desc"));
// console.log(Person.sort(p, "age", "asc"));
// console.log(Person.sort(p, "age", "desc"));
// console.log(Person.sort(p, "salary", "asc"));
// console.log(Person.sort(p, "salary", "desc"));
// console.log(Person.sort(p, "male", "asc"));
// console.log(Person.sort(p, "female", "desc"));



console.log("***********************Original Array***********************************");
//Original array
console.log(p);


/*
For sex field I have done something like if user wants all the Person who are male then it can get it by using 
   console.log(Person.sort(p,"male","asc"));

   for female 
   console.log(Person.sort(p,"male","asc"));


   Here the order dosen't matter.


*/
